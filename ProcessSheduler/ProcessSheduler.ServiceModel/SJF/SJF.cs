﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessSheduler.ServiceModel
{
    public class SJF
    {
        public List<Process> Pending { get; set; }
        public List<Process> Processes { get; set; }
        public List<Process> Served { get; set; }
        public Clock Clock { get; set; }

        public void Run()
        {
            Clock.Reset();
            MoveProcessToQueue();
            while (Processes.Count != 0)
            {
                var current = Processes[0];
                Processes.RemoveAt(0);
                current.Waited = Clock.Time - current.Started;
                for (int i = 0; i < current.TimeToServe; i++)
                {
                    Clock.Time++;
                    if (!Pending.IsEmpty<Process>() && Pending[0].TimeIn == Clock.Time)
                    {
                        MoveProcessToQueue();
                    }
                }


                Served.Add(current);
            }
        }

        private void MoveProcessToQueue()
        {
            var process = Pending[0];
            Processes.Add(process);
            process.Started = Clock.Time;

            Processes.Sort();
            Pending.RemoveAt(0);
        }

        private double CalculateAverage ()
        {
            if (Processes.Count == 0) return 0;
            long sum = 0;
            foreach (Process p in Processes)
            {
                sum += Clock.Time - p.Started;
            }
            return sum / Processes.Count;
        }
    }
}
