﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessSheduler.ServiceModel
{
    public class Results
    {
        public List<ProcessResult> Processes { get; set; }
    }
}
