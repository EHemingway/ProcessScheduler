﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessSheduler.ServiceModel
{
    public class Clock
    {
        public long Time { get; set; }

        public void PassTime (long timePassed)
        {
            Time += timePassed;
        }

        public void Reset ()
        {
            Time = 0;
        }
    }
}
