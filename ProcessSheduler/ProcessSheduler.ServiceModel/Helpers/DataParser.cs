﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessSheduler.ServiceModel.Helpers
{
    public class DataParser
    {
        public string FilePath { get; set; }

        public List<Process> GetInputData ()
        {
            List<Process> processes = new List<Process>();

            string[] lines = System.IO.File.ReadAllLines(FilePath);
            foreach (string line in lines)
            {
                string[] parts = line.Split(' ');
                processes.Add(new Process
                {
                    Id = parts[0],
                    TimeIn = Int32.Parse(parts[1]),
                    TimeToServe = Int32.Parse(parts[2])
                });
                
            }
            return processes;
        }


    }
}
