﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessSheduler.ServiceModel
{
    public class Process : IComparable<Process>
    {
        public string Id { get; set; }
        public long TimeIn { get; set; }
        public long TimeToServe { get; set; }
        public long Started { get; set; }
        public long Waited { get; set; }

        public Process()
        {
            Waited = 0;
        }

        public int CompareTo(Process other)
        {
            return TimeToServe.CompareTo(other.TimeToServe);
        }
    }
}
