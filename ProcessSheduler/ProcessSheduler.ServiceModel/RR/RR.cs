﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessSheduler.ServiceModel
{
    public class RR
    {
        public List<Process> Pending { get; set; }
        public Queue<Process> Processes { get; set; }
        public List<Process> Served { get; set; }
        public Clock Clock { get; set; }
        public long TimeQuant { get; set; }

        public void Run()
        {
            Clock.Reset();
            MoveProcessToQueue();
            while (Processes.Count != 0)
            {
                var current = Processes.Dequeue();
                current.Waited += Clock.Time - current.Started;

                for (int i = 0; i < TimeQuant && current.TimeToServe != 0; i++)
                {
                    Clock.Time++;
                    current.TimeToServe--;
                    if (!Pending.IsEmpty<Process>() && Pending[0].TimeIn == Clock.Time)
                    {
                        MoveProcessToQueue();
                    }
                }
                if (current.TimeToServe == 0)
                {
                    Served.Add(current);
                }
                else
                {
                    current.Started = Clock.Time;
                    Processes.Enqueue(current);
                }
            }
        }

        private void MoveProcessToQueue()
        {
            var process = Pending[0];
            Processes.Enqueue(process);
            process.Started = Clock.Time;
            Pending.RemoveAt(0);
        }

        private double CalculateAverage()
        {
            if (Processes.Count == 0) return 0;
            long sum = 0;
            foreach (Process p in Processes)
            {
                sum += Clock.Time - p.Started;
            }
            return sum / Processes.Count;
        }
    }
}
