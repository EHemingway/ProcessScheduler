﻿using ProcessSheduler.ServiceModel;
using ProcessSheduler.ServiceModel.Helpers;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessSheduler.ServiceInterface
{
    class FCFSService : Service
    {
        public object Any(FCFSController request)
        {
            DataParser inputParser = new DataParser { FilePath = "C:\\Users\\Pawel\\Documents\\visual studio 2015\\Projects\\ProcessSheduler\\ProcessSheduler\\ProcessSheduler.ServiceModel\\Resources\\FCFS_POS.txt" };

            var simulation = new FCFS
            {
                Clock = new Clock(),
                Pending = inputParser.GetInputData(),
                Processes = new Queue<Process>(),
                Served = new List<Process>()
            };

            simulation.Run();

            var results = new Results();
            
            

            return results;
        }

        public object Any(FCFSnegController request)
        {
            DataParser inputParser = new DataParser { FilePath = "C:\\Users\\Pawel\\Documents\\visual studio 2015\\Projects\\ProcessSheduler\\ProcessSheduler\\ProcessSheduler.ServiceModel\\Resources\\FCFS_NEG.txt" };

            var simulation = new FCFS
            {
                Clock = new Clock(),
                Pending = inputParser.GetInputData(),
                Processes = new Queue<Process>(),
                Served = new List<Process>()
            };

            simulation.Run();

            

            var results = new Results();


            return results;
        }
    }
}
