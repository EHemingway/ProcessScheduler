﻿using ProcessSheduler.ServiceModel;
using ProcessSheduler.ServiceModel.Helpers;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessSheduler.ServiceInterface
{
    public class RRService : Service
    {
        public Object Any(RRController request)
        {
            DataParser inputParser = new DataParser { FilePath = "C:\\Users\\Pawel\\Documents\\visual studio 2015\\Projects\\ProcessSheduler\\ProcessSheduler\\ProcessSheduler.ServiceModel\\Resources\\FCFS_POS.txt" };

            List<double> averages = new List<double>();

            for(int i = 0; i < 1000; i ++)
            {
                var simulation = new RR
                {
                    Clock = new Clock(),
                    Pending = inputParser.GetInputData(),
                    Processes = new Queue<Process>(),
                    Served = new List<Process>(),
                    TimeQuant = 50
                };

                simulation.Run();

                long sum = 0;
                foreach (Process si in simulation.Served)
                {
                    sum += si.Waited;
                }

                averages.Add(sum / simulation.Served.Count);
            }

            double s = 0;

            foreach(long l in averages)
            {
                s += l;
            }

            double av = s / averages.Count;

            var results = new Results();

            return results;
        }
    }
}
