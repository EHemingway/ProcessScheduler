﻿using ProcessSheduler.ServiceModel;
using ProcessSheduler.ServiceModel.Helpers;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessSheduler.ServiceInterface
{
    public class SJFwService : Service
    {
        public Object Any(SJFwController request)
        {
            DataParser inputParser = new DataParser { FilePath = "C:\\Users\\Pawel\\Documents\\visual studio 2015\\Projects\\ProcessSheduler\\ProcessSheduler\\ProcessSheduler.ServiceModel\\Resources\\FCFS_NEG.txt" };

            var simulation = new SJFw
            {
                Clock = new Clock(),
                Pending = inputParser.GetInputData(),
                Processes = new List<Process>(),
                Served = new List<Process>(),
                Wyw = 0
            };

            simulation.Run();

            long sum = 0;
            foreach (Process s in simulation.Served)
            {
                sum += s.Waited;
            }

            double av = sum / simulation.Served.Count;

            var results = new Results();

            return results;
        }
    }
}
